# Changelog MedReg

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

## [5.1.0.6] - 2017-11-28
### Fixed
* **Menu Registrasi Pasien** 
* - Fix Generate No. RM Pasien dalam jaringan multi user.

## [5.1.0.5] - 2017-11-19
### Added
* **Menu Registrasi Pasien** 
*    - Bisa menghapus data pasien yang belum digunakan untuk transaksi.
### Changed
* **Menu Group User** 
*    - Update dengan sistem terbaru.
* **Menu Registrasi Pasien** 
*    - Update fungsi GoToRecord > MoveBy untuk membantu pencarian data pasien
*    - Update fungsi Load Data Pasien
### Fixed
* **Menu Registrasi Pasien** 
*    - Fix Generate No. RM Pasien.